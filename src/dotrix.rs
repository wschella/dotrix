// Copyright (c) 2015, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use std::collections::{HashMap, HashSet};
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::os::unix;
use std::path::{Path, PathBuf};
use toml;

#[derive(Debug)]
pub struct ParserErrors {
    pub file: PathBuf,
    pub errors: Vec<toml::ParserError>,
}

impl ParserErrors {
    fn new<T: Into<PathBuf>>(file: T, errors: Vec<toml::ParserError>) -> Self {
        ParserErrors {
            file: file.into(),
            errors: errors,
        }
    }
}

#[derive(Debug)]
pub struct IoError {
    pub file: PathBuf,
    pub error: io::Error,
}

impl IoError {
    fn new<T: Into<PathBuf>>(file: T, error: io::Error) -> Self {
        IoError {
            file: file.into(),
            error: error,
        }
    }
}

#[derive(Debug)]
pub struct InvalidDotTomlError {
    pub file: PathBuf,
    pub desc: String,
}

impl InvalidDotTomlError {
    fn new<T: Into<PathBuf>>(file: T, desc: String) -> Self {
        InvalidDotTomlError {
            file: file.into(),
            desc: desc,
        }
    }
}

#[derive(Debug)]
pub enum DotrixError {
    ParserErrors(ParserErrors),
    IoError(IoError),
    InvalidDotTomlError(InvalidDotTomlError),
}

impl From<ParserErrors> for DotrixError {
    fn from(e: ParserErrors) -> Self {
        DotrixError::ParserErrors(e)
    }
}

impl From<IoError> for DotrixError {
    fn from(e: IoError) -> Self {
        DotrixError::IoError(e)
    }
}

impl From<InvalidDotTomlError> for DotrixError {
    fn from(e: InvalidDotTomlError) -> Self {
        DotrixError::InvalidDotTomlError(e)
    }
}

pub type DotrixResult<T> = Result<T, DotrixError>;

fn parse_toml<T: AsRef<Path>, U: AsRef<Path>>(src: T, dottoml_file: U) -> DotrixResult<toml::Table> {
    let src = src.as_ref();
    let dottoml_file = dottoml_file.as_ref();

    let mut f = try!(File::open(src).map_err(|e| IoError::new(dottoml_file, e)));
    let mut dottoml_str = String::new();
    try!(f.read_to_string(&mut dottoml_str).map_err(|e| IoError::new(dottoml_file, e)));

    let mut parser = toml::Parser::new(&dottoml_str);
    parser.parse().ok_or_else(move || ParserErrors::new(dottoml_file, parser.errors).into())
}

#[derive(Debug)]
pub struct Dotrix {
    ignore: HashSet<PathBuf>,
    src_ignore: HashSet<PathBuf>,
    targets: HashMap<PathBuf, DotrixTarget>,
    known_files: HashSet<PathBuf>,
    symlinks: Vec<SymLink>,
}

impl Dotrix {
    pub fn new() -> Self {
        Dotrix {
            ignore: HashSet::new(),
            src_ignore: HashSet::new(),
            targets: HashMap::new(),
            known_files: HashSet::new(),
            symlinks: Vec::new(),
        }
    }

    fn read_root_table<T: AsRef<Path>>(&mut self, mut dottoml: toml::Table, dottoml_file: T) -> Option<toml::Table> {
        let dottoml_file = dottoml_file.as_ref();

        match dottoml.remove("dotrix") {
            Some(toml::Value::Table(t)) => { Some(t) }

            _ => {
                println!("Warning ({}): missing dotrix table.", dottoml_file.display());
                None
            }
        }
    }

    fn add_ignore_table<T: AsRef<Path>, U: AsRef<Path>>(&mut self, root: &mut toml::Table, prefix: T, dottoml_file: U) -> DotrixResult<()> {
        let prefix = prefix.as_ref();
        let dottoml_file = dottoml_file.as_ref();

        match root.remove("ignore") {
            Some(toml::Value::Array(arr)) => {
                for elem in arr {
                    try!(self.add_ignore_element(elem, prefix, &dottoml_file));
                }
            }

            None => { }

            _ => {
                return Err(DotrixError::InvalidDotTomlError(InvalidDotTomlError::new(
                    &dottoml_file,
                    "dotrix.ignore is not a list of strings".to_owned())));
            }
        };

        Ok(())
    }

    fn add_ignore_element<T: AsRef<Path>, U: AsRef<Path>>(&mut self, elem: toml::Value, prefix: T, dottoml_file: U) -> DotrixResult<()> {
        let prefix = prefix.as_ref();
        let dottoml_file = dottoml_file.as_ref();

        match elem {
            toml::Value::String(s) => {
                let ignored_file = prefix.join(&s);
                if !self.ignore.insert(ignored_file.clone()) {
                    println!("Warning ({}): {} is ignored multiple times.", dottoml_file.display(), s);
                }
            }

            _ => {
                return Err(DotrixError::InvalidDotTomlError(InvalidDotTomlError::new(
                    &dottoml_file,
                    "dotrix.ignore is not a list of strings".to_owned())));
            }
        }

        Ok(())
    }

    fn add_link_table<T: AsRef<Path>, U: AsRef<Path>>(&mut self, root: &mut toml::Table, prefix: T, dottoml_file: U) -> DotrixResult<()> {
        let prefix = prefix.as_ref();
        let dottoml_file = dottoml_file.as_ref();

        match root.remove("link") {
            Some(toml::Value::Table(tbl)) => {
                for (key, value) in tbl {
                    let src: PathBuf = key.into();

                    if self.targets.contains_key(&src) {
                        let target = self.targets.remove(&src).unwrap();
                        println!("Warning ({}): {} is listed multiple times. Ignoring all future occurrences.",
                                 target.dottoml_file.display(), src.display());
                        self.src_ignore.insert(src.clone());
                    }

                    if self.src_ignore.contains(&src) {
                        println!("Warning ({}): {} is listed multiple times. Ignoring all future occurrences.",
                                 dottoml_file.display(), src.display());
                        continue;
                    }

                    try!(self.add_link_element(src, value, prefix, &dottoml_file));
                }
            }

            None => { }

            _ => {
                return Err(DotrixError::InvalidDotTomlError(InvalidDotTomlError::new(
                    &dottoml_file,
                    "dotrix.link is not a table".to_owned())));
            }
        };

        Ok(())
    }

    fn add_link_element<T: AsRef<Path>, U: AsRef<Path>, V: AsRef<Path>>(&mut self, src: T, value: toml::Value, prefix: U, dottoml_file: V) -> DotrixResult<()> {
        let src = src.as_ref();
        let prefix = prefix.as_ref();
        let dottoml_file = dottoml_file.as_ref();

        match value {
            toml::Value::String(s) => {
                let dst = prefix.join(s);

                if !self.ignore.contains(&dst) {
                    self.known_files.insert(dst.clone());
                    self.targets.insert(src.into(), DotrixTarget::new(dottoml_file, dst));
                }
                else {
                    println!("Warning ({}): {} is ignored but also listed in dotrix.link table.",
                             dottoml_file.display(), dst.display());
                }
            }

            _ => {
                return Err(DotrixError::InvalidDotTomlError(InvalidDotTomlError::new(
                    &dottoml_file,
                    format!("dotrix.link.{} is not a string", src.display()))));
            }
        }

        Ok(())
    }

    pub fn add_dottoml<T: AsRef<Path>, U: AsRef<Path>>(&mut self, src: T, prefix: U) -> DotrixResult<()> {
        let prefix = prefix.as_ref();
        let dottoml_file = prefix.join("dot.toml");

        let table = try!(parse_toml(&src, &dottoml_file));
        let mut root = match self.read_root_table(table, &dottoml_file) {
            Some(t) => t,
            None => { return Ok(()); }
        };

        try!(self.add_ignore_table(&mut root, prefix, &dottoml_file));
        try!(self.add_link_table(&mut root, prefix, &dottoml_file));

        Ok(())
    }

    pub fn check_file<T: AsRef<Path>, U: AsRef<Path>>(&mut self, file: T, prefix: U) {
        let file = file.as_ref();
        let prefix = prefix.as_ref();
        let key = prefix.join(file);

        if !self.ignore.contains(&key) && !self.known_files.contains(&key) {
            let dottoml_file = prefix.join("dot.toml");
            println!("Warning ({}): {} exists but is neither ignored nor in dotrix.link table.", dottoml_file.display(), file.display());
        }
    }

    pub fn convert_to_symlinks<T: AsRef<Path>, U: AsRef<Path>>(&mut self, src_path: T, dst_path: U) {
        let src_path = src_path.as_ref();
        let dst_path = dst_path.as_ref();

        for (key, target) in &self.targets {
            let link_src = dst_path.join(key);
            let link_dst = src_path.join(&target.dst);

            match fs::metadata(&link_dst) {
                Ok(_) => { }

                Err(e) => {
                    match e.kind() {
                        io::ErrorKind::NotFound => {
                            println!("Warning ({}): {} doesn't exist.", target.dottoml_file.display(), target.dst.display());
                        }

                        _ => {
                            println!("Warning ({}): failed to stat {} ({}).",
                                     target.dottoml_file.display(), target.dst.display(), e);
                            continue;
                        }
                    }
                }
            }

            {
                let parent = link_src.parent().unwrap();
                match fs::metadata(&parent) {
                    Ok(meta) => {
                        if !meta.is_dir() {
                            println!("Warning ({}): skipping {} because the destinations parent ({}) is not a directory.",
                                     target.dottoml_file.display(), target.dst.display(), parent.display());
                            continue;
                        }
                    }

                    Err(e) => {
                        match e.kind() {
                            io::ErrorKind::NotFound => {
                                println!("Warning ({}): skipping {} because the destinations parent ({}) doesn't exist.",
                                         target.dottoml_file.display(), target.dst.display(), parent.display());
                                continue;
                            }

                            _ => {
                                println!("Warning ({}): skipping {} because of a failure while reading the destinations parent ({}) ({}).",
                                         target.dottoml_file.display(), target.dst.display(), parent.display(), e);
                                continue;
                            }
                        }
                    }
                }
            }

            match fs::read_link(&link_src) {
                Ok(link) => {
                    if link_dst != link {
                        println!("Warning ({}): skipping {} because {} exists already and has a different destination.",
                                 target.dottoml_file.display(), target.dst.display(), link_src.display());
                        continue;
                    }
                    else {
                        continue;
                    }
                }

                Err(e) => {
                    match e.kind() {
                        io::ErrorKind::NotFound => { }

                        io::ErrorKind::InvalidInput => {
                            println!("Warning ({}): skipping {} because {} exists and is not a symlink.",
                                     target.dottoml_file.display(), target.dst.display(), link_src.display());
                            continue;
                        }

                        _ => {
                            println!("Warning ({}): skipping {} because of a failure while reading the symlink {} ({}).",
                                     target.dottoml_file.display(), target.dst.display(), link_src.display(), e);
                            continue;
                        }
                    }
                }
            }

            self.symlinks.push(SymLink::new(link_src, link_dst));
        }
    }

    pub fn run(&self, dryrun: bool) -> bool {
        for symlink in &self.symlinks {
            if !dryrun {
                match unix::fs::symlink(&symlink.dst, &symlink.src) {
                    Ok(_) => { }

                    Err(e) => {
                        println!("Error: failed to create symlink {} -> {} ({}).", symlink.dst.display(), symlink.src.display(), e);
                        return false;
                    }
                }
            }

            println!("Symlinked: {} -> {}.", symlink.src.display(), symlink.dst.display());
        }

        return true;
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct DotrixTarget {
    dottoml_file: PathBuf,
    dst: PathBuf,
}

impl DotrixTarget {
    fn new<T: Into<PathBuf>, U: Into<PathBuf>>(dottoml_file: T, dst: U) -> Self {
        DotrixTarget {
            dottoml_file: dottoml_file.into(),
            dst: dst.into(),
        }
    }
}

#[derive(Debug)]
struct SymLink {
    src: PathBuf,
    dst: PathBuf,
}

impl SymLink {
    fn new<T: Into<PathBuf>, U: Into<PathBuf>>(src: T, dst: U) -> Self {
        SymLink { src: src.into(), dst: dst.into() }
    }
}
